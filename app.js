var express = require('express');
var port = process.env.PORT || 3000;
var app = express();

app.get('/', function(request, response) {
    response.sendFile(__dirname + '/src/index.html');
}).listen(port);

app.use(express.static('src'));
