import { EventEmitter } from "events";

import dispatcher from "../dispatcher";

class RandomPeopleStore extends EventEmitter {
  constructor() {
    super();
    this.randoms = [];
    this.loading = false;
    this.errors = [];
  }

  getRandoms() {
    return {
      randoms: this.randoms,
      loading: this.loading,
      errors: this.errors
    };
  }

  setRandoms(randoms) {
    this.loading = false;
    this.randoms = randoms;
    this.emit("change");
  }

  setLoading() {
    this.loading = true;
    this.randoms = [];
    this.emit("change");
  }

  setError(error) {
    this.errors.push(error);
    this.loading = false;

    setTimeout(() => {
      this.errors = [];
      this.emit("change");
    }, 5000);

    this.emit("change");
  }

  handleActions(action) {
    console.log(action);
    switch(action.type) {
      case "LOAD_RANDOM": {
        this.setLoading();
        break;
      }
      case "REQUEST_SUCCESS": {
        this.setRandoms(action.response.data.results);
        break;
      }
      case "REQUEST_ERROR": {
        this.setError(action.error);
        break;
      }
    }
  }
}

const randomPeopleStore = new RandomPeopleStore;

dispatcher.register(randomPeopleStore.handleActions.bind(randomPeopleStore));

export default randomPeopleStore;
