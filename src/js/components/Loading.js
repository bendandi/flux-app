import React from "react";

export default class Loading extends React.Component {
  render() {
    return(
      <div>
        {this.props.loading ? <img class="loading" src="../../img/loading.svg" /> : null}
      </div>
    );
  }
}

Loading.propTypes = {
  loading: React.PropTypes.bool.isRequired
}
