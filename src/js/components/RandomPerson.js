import React from "react";

export default class RandomPerson extends React.Component {
  constructor() {
    super();
    this.state = {display: 'hidden'};
  }

  componentWillMount() {
    setTimeout(() => {
      this.setState({display: 'transition-in'})
    }, this.props.displayDelay);
  }

  render() {
    const {name, picture, email} = this.props;

    return(
      <li className={`${this.state.display} media`}>
        <img src={picture.thumbnail}/>
        <div class="media-content">
          <div  class="media-content-header">{name.first + " " + name.last}</div>
          <div class="media-content-body">{email}</div>
        </div>
      </li>
    );
  }
}
