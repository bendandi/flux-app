import React from "react";

export default class Errors extends React.Component {


  render() {
    const { errors } = this.props;
    const errorListItems = this.props.errors.map((error, index) => {
      return <li key={index}>{error.message}</li>;
    });
    
    return(
      <ul>{errorListItems}</ul>
    );
  }
}
