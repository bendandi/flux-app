import React from 'react/addons';
import expect from 'expect';
import RandomPeople from '../pages/RandomPeople';

const TestUtils = React.addons.TestUtils;

describe('RandomPeople', () => {
  it("it renders", () => {
    const randomPeople = TestUtils.renderIntoDocument(<RandomPeople />);
    expect(randomPeople).toBeTruthy();
  });
});
