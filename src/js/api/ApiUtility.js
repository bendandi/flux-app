import * as RandomPeopleActions from "../actions/RandomPeopleActions";
import axios from "axios";

export function get() {
  const config = {
    headers: {
             'Accept': 'application/json'
       }
  };

  axios.get("https://api.randomuser.me/?results=10", config)
    .then((data) => {
      RandomPeopleActions.HandleRequestSuccess(data);
    })
    .catch((error) => {
      RandomPeopleActions.HandleRequestError(error);
    });
}
