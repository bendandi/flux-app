import React from "react";

import Errors from "../components/Errors";
import Loading from "../components/Loading"
import RandomPerson from "../components/RandomPerson";
import * as RandomPeopleActions from "../actions/RandomPeopleActions";
import RandomPeopleStore from "../stores/RandomPeopleStore";

export default class RandomPeople extends React.Component {
  constructor() {
    super();
    this.state = {
      randoms: [],
      loading: false,
      errors: []
    }
  }

  componentWillMount() {
    RandomPeopleStore.on("change", this.getRandoms.bind(this));
  }

  componentWillUnmount() {
    RandomPeopleStore.removeListener("change", this.getRandoms.bind(this));
  }

  getRandoms() {
    const {randoms, loading, errors } = RandomPeopleStore.getRandoms();
    this.setState({
      randoms : randoms,
      loading : loading,
      errors : errors,
    });
  }

  loadRandom() {
      RandomPeopleActions.loadRandom();
  }

  render() {
    const RandomPersonComponents = this.state.randoms.map((random, i) => {
      return <RandomPerson key={random.email} displayDelay={i * 100} {...random} />;
    });

    return (
      <div>
        <div class="button" onClick={this.loadRandom.bind(this)}>Load Randoms</div>
        <Errors errors={this.state.errors}/>
        <ul>{RandomPersonComponents}</ul>
        <Loading loading={this.state.loading}/>
      </div>
    );
  }
}
