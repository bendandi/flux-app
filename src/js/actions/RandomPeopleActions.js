import dispatcher from "../dispatcher";
import * as ApiUtility from "../api/ApiUtility"

export function loadRandom() {
  dispatcher.dispatch({
    type: "LOAD_RANDOM",
  });
  ApiUtility.get();
}

export function HandleRequestSuccess(response) {
  dispatcher.dispatch({
    type: "REQUEST_SUCCESS",
    response: response,
  });
}

export function HandleRequestError(error) {
  dispatcher.dispatch({
    type: "REQUEST_ERROR",
    error: error,
  });
}
